﻿Imports System.Data.SqlClient
Public Class Modulo1
    Dim strCon As String = "Data Source=6N;Initial Catalog=SIGFOT;Integrated Security=True"
    Dim con As New SqlConnection
    Dim command As SqlCommand
    Dim strCommand As String
    Public Last As Integer
    Private Sub btnRegresar_Click(sender As Object, e As EventArgs) Handles btnRegresar.Click
        MenuPrincipal.Show()
        Me.Dispose()
    End Sub

    Private Sub Modulo1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'SIGFOTDataSet2.ELEMENTO' Puede moverla o quitarla según sea necesario.
        Me.ELEMENTOTableAdapter1.Fill(Me.SIGFOTDataSet2.ELEMENTO)


        dgModulo1.Columns(0).Width = 50
        dgModulo1.Columns(1).Width = 90
        dgModulo1.Columns(2).Width = 500
        dgModulo1.Columns(3).Width = 60
        dgModulo1.Columns(4).Width = 60

        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "select * from ELEMENTO"
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            con.Open()
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(comando)

            da.Fill(dt)

            dgModulo1.DataSource = dt




        Catch ex As Exception
            MessageBox.Show("Error Carga De Datos")
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub tbBuscar_TextChanged(sender As Object, e As EventArgs) Handles tbBuscar.TextChanged
        If tbBuscar.Text = "" Then
            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select * from ELEMENTO"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)

                da.Fill(dt)

                dgModulo1.DataSource = dt




            Catch ex As Exception
                MessageBox.Show("Error Al Buscar Datos")
            Finally
                con.Close()
            End Try

        Else

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select * from ELEMENTO where item like '%" & tbBuscar.Text & "%' 
                or area like '%" & tbBuscar.Text & "%'
                or descripcion like '%" & tbBuscar.Text & "%'
                or unidad like '%" & tbBuscar.Text & "%'
                or cumm like '%" & tbBuscar.Text & "%'
                or cusmo like '%" & tbBuscar.Text & "%'"


                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)
                da.Fill(dt)
                dgModulo1.DataSource = dt




            Catch ex As Exception
                MessageBox.Show("Error De Busqueda")
            Finally
                con.Close()
            End Try

        End If
    End Sub

    Private Sub dgModulo1_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgModulo1.CellMouseDown
        If e.Button = MouseButtons.Right Then

            If e.RowIndex < 0 Or e.ColumnIndex < 0 Then Exit Sub
            dgModulo1.CurrentCell = dgModulo1(e.ColumnIndex, e.RowIndex)
            ' Maybe modify the text on your context menu options here?
        End If
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Me.Hide()
        AgregarM1.Show()
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuItem.Click
        Me.Hide()
        ModificarM1.Show()

        Dim row As DataGridViewRow = dgModulo1.CurrentRow
        ModificarM1.tbItem.Text = CStr(row.Cells(0).Value)
        ModificarM1.tbArea.Text = CStr(row.Cells(1).Value)
        ModificarM1.tbDescripcion.Text = CStr(row.Cells(2).Value)
        ModificarM1.tbUnidad.Text = CStr(row.Cells(3).Value)
        ModificarM1.tbCostoMM.Text = CStr(row.Cells(4).Value)
        ModificarM1.tbCostoSMO.Text = CStr(row.Cells(5).Value)

        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "Select CodElemento From Elemento Where Item='" & ModificarM1.tbItem.Text & "'"
            '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
            Dim comando As SqlCommand
            Dim rs As SqlDataReader
            con.Open()
            comando = New SqlCommand(sql, con)
            rs = comando.ExecuteReader
            rs.Read()
            Last = rs(0)
            rs.Close()

        Catch ex As Exception
            MessageBox.Show("Error Carga Datos Modificar")
        Finally
            con.Close()
        End Try

    End Sub
    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EliminarToolStripMenuItem.Click
        Dim YesOrNoAnswerToMessageBox As String
        Dim QuestionToMessageBox As String
        Dim row As DataGridViewRow = dgModulo1.CurrentRow

        QuestionToMessageBox = "Esta seguro que desea eliminar el elemento permanentemente?"

        YesOrNoAnswerToMessageBox = MsgBox(QuestionToMessageBox, vbYesNo, "Eliminando")

        If YesOrNoAnswerToMessageBox = vbNo Then

        Else
            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "Select CodElemento From Elemento Where item='" & CStr(row.Cells(0).Value) & "'"
                '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
                Dim comando As SqlCommand
                Dim rs As SqlDataReader
                con.Open()
                comando = New SqlCommand(sql, con)
                rs = comando.ExecuteReader
                rs.Read()
                Last = rs(0)
                rs.Close()

            Catch ex As Exception
                MessageBox.Show("Error Selección Dato A Eliminar")
            Finally
                con.Close()
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "DELETE FROM elemento WHERE codelemento='" & Last & "'"

                '"Select CodElemento From Elementos Where Item='" & ModificarM1.tbItem.Text & "'"
                '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                comando.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show("Error Eliminación")
            Finally
                con.Close()
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select * from elemento"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)

                da.Fill(dt)

                dgModulo1.DataSource = dt




            Catch ex As Exception
                'MessageBox.Show("Error")
            Finally
                tbBuscar.Text = ""
                MessageBox.Show("Eliminado!", "Eliminando", MessageBoxButtons.OK, MessageBoxIcon.Information)
                con.Close()
            End Try

        End If



    End Sub

    Private Sub VisualizarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VisualizarToolStripMenuItem.Click
        Me.Hide()
        VisualizarM1.Show()

        Dim row As DataGridViewRow = dgModulo1.CurrentRow
        VisualizarM1.tbItem.Text = CStr(row.Cells(0).Value)
        VisualizarM1.tbArea.Text = CStr(row.Cells(1).Value)
        VisualizarM1.tbDescripcion.Text = CStr(row.Cells(2).Value)
        VisualizarM1.tbUnidad.Text = CStr(row.Cells(3).Value)
        VisualizarM1.tbCostoMM.Text = CStr(row.Cells(4).Value)
        VisualizarM1.tbCostoSMO.Text = CStr(row.Cells(5).Value)

    End Sub

    Private Sub Modulo1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Salir(e)
    End Sub
End Class