﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModificarM1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbCostoSMO = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbUnidad = New System.Windows.Forms.TextBox()
        Me.tbArea = New System.Windows.Forms.TextBox()
        Me.tbCostoMM = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnRegresar = New System.Windows.Forms.Button()
        Me.tbDescripcion = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbItem = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'tbCostoSMO
        '
        Me.tbCostoSMO.Location = New System.Drawing.Point(273, 141)
        Me.tbCostoSMO.Name = "tbCostoSMO"
        Me.tbCostoSMO.Size = New System.Drawing.Size(251, 20)
        Me.tbCostoSMO.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(58, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(209, 13)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "COSTO UNITARIO SIN MANO DE OBRA:"
        '
        'tbUnidad
        '
        Me.tbUnidad.Location = New System.Drawing.Point(71, 54)
        Me.tbUnidad.Name = "tbUnidad"
        Me.tbUnidad.Size = New System.Drawing.Size(176, 20)
        Me.tbUnidad.TabIndex = 2
        '
        'tbArea
        '
        Me.tbArea.Location = New System.Drawing.Point(350, 15)
        Me.tbArea.Name = "tbArea"
        Me.tbArea.Size = New System.Drawing.Size(176, 20)
        Me.tbArea.TabIndex = 1
        '
        'tbCostoMM
        '
        Me.tbCostoMM.Location = New System.Drawing.Point(273, 109)
        Me.tbCostoMM.Name = "tbCostoMM"
        Me.tbCostoMM.Size = New System.Drawing.Size(253, 20)
        Me.tbCostoMM.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(254, 13)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "COSTO UNITARIO MANO DE OBRA + MATERIAL:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 57)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "UNIDAD:"
        '
        'btnRegresar
        '
        Me.btnRegresar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRegresar.Location = New System.Drawing.Point(381, 283)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(143, 56)
        Me.btnRegresar.TabIndex = 7
        Me.btnRegresar.Text = "Regresar"
        Me.btnRegresar.UseVisualStyleBackColor = True
        '
        'tbDescripcion
        '
        Me.tbDescripcion.Location = New System.Drawing.Point(115, 178)
        Me.tbDescripcion.Multiline = True
        Me.tbDescripcion.Name = "tbDescripcion"
        Me.tbDescripcion.Size = New System.Drawing.Size(411, 83)
        Me.tbDescripcion.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 181)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "DESCRIPCION:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(305, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "AREA:"
        '
        'tbItem
        '
        Me.tbItem.Location = New System.Drawing.Point(71, 12)
        Me.tbItem.Name = "tbItem"
        Me.tbItem.ReadOnly = True
        Me.tbItem.Size = New System.Drawing.Size(176, 20)
        Me.tbItem.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "ITEM:"
        '
        'btnModificar
        '
        Me.btnModificar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnModificar.Location = New System.Drawing.Point(215, 283)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(143, 56)
        Me.btnModificar.TabIndex = 6
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'ModificarM1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(543, 359)
        Me.Controls.Add(Me.tbCostoSMO)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbUnidad)
        Me.Controls.Add(Me.tbArea)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.tbCostoMM)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnRegresar)
        Me.Controls.Add(Me.tbDescripcion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbItem)
        Me.Controls.Add(Me.Label1)
        Me.Name = "ModificarM1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Modificar Elemento"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tbCostoSMO As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents tbUnidad As TextBox
    Friend WithEvents tbArea As TextBox
    Friend WithEvents tbCostoMM As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents btnRegresar As Button
    Friend WithEvents tbDescripcion As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents tbItem As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnModificar As Button
End Class
