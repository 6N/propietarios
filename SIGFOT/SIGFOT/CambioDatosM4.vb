﻿Imports System.IO
Public Class CambioDatosM4
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Modulo4.Show()
        Me.Dispose()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If tbCAnt.Text = "" Or tbNCon.Text = "" Or tbRepetir.Text = "" Or tbUsuario.Text = "" Then
            MessageBox.Show("Faltan uno o algunos campos por llenar!", "Campos Vacios", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        Else
            If tbCAnt.Text = b Then
                If tbNCon.Text = tbRepetir.Text Then
                    Dim Archivo As System.IO.FileStream
                    Archivo = System.IO.File.Create(ruta + "\archivo.txt")
                    Archivo.Close()

                    If File.Exists(ruta + "\archivo.txt") Then
                        Dim oSW As New StreamWriter(ruta + "\archivo.txt")
                        Dim Linea As String
                        Linea = tbUsuario.Text & vbNewLine & tbRepetir.Text
                        oSW.WriteLine(Linea)
                        oSW.Flush()
                        oSW.Close()
                        Modulo4.Show()
                        Me.Hide()

                    Else
                        MsgBox("No se pudo guardar los datos", vbInformation, "Mi programa X")
                    End If

                    MessageBox.Show("Contraseña y Usuarios Cambiados!!", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("La contraseñas nuevas no coinciden!", "Error Contraseñas Nuevas", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("La contraseña anterior no coincide", "Error Contraseña Antigua", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End If
    End Sub

    Private Sub tbCAnt_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbCAnt.KeyPress

        Me.tbCAnt.Text = Trim(Replace(Me.tbCAnt.Text, "  ", " "))
        tbCAnt.Select(tbCAnt.Text.Length, 0)
    End Sub

    Private Sub tbUsuario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbUsuario.KeyPress

        Me.tbUsuario.Text = Trim(Replace(Me.tbUsuario.Text, "  ", " "))
        tbUsuario.Select(tbUsuario.Text.Length, 0)
    End Sub

    Private Sub tbNCon_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbNCon.KeyPress

        Me.tbNCon.Text = Trim(Replace(Me.tbNCon.Text, "  ", " "))
        tbNCon.Select(tbNCon.Text.Length, 0)
    End Sub

    Private Sub tbRepetir_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbRepetir.KeyPress

        Me.tbRepetir.Text = Trim(Replace(Me.tbRepetir.Text, "  ", " "))
        tbRepetir.Select(tbRepetir.Text.Length, 0)
    End Sub

    Private Sub CambioDatosM4_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tbCAnt.UseSystemPasswordChar = True
        tbNCon.UseSystemPasswordChar = True
        tbRepetir.UseSystemPasswordChar = True
    End Sub
End Class