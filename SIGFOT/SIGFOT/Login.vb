﻿Imports System.IO
Public Class Login

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click

        If tbUsuario.Text = a And tbContraseña.Text = b Then
            MenuPrincipal.Show()
            Me.Close()
        Else
            MessageBox.Show("Contraseña o usuario incorrectos!")
            tbContraseña.Text = ""
            tbUsuario.Text = ""
            tbUsuario.Focus()
        End If

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            tbContraseña.UseSystemPasswordChar = False
        Else
            tbContraseña.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tbContraseña.UseSystemPasswordChar = True
        tbUsuario.Select()
        Try
            If File.Exists(ruta + "\archivo.txt") Then
                'cargamos los datos del archivo
                cargar()
            Else
                Exit Sub
            End If
        Catch oe As Exception
            MsgBox(oe.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub tbContraseña_KeyDown(sender As Object, e As KeyEventArgs) Handles tbContraseña.KeyDown
        Select Case e.KeyData
            Case Keys.Enter
                btnIngresar.PerformClick()
        End Select
    End Sub

    Private Sub tbUsuario_KeyDown(sender As Object, e As KeyEventArgs) Handles tbUsuario.KeyDown
        Select Case e.KeyData
            Case Keys.Enter
                btnIngresar.PerformClick()
        End Select
    End Sub


End Class
