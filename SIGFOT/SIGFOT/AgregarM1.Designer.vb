﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AgregarM1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbItem = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbDescripcion = New System.Windows.Forms.TextBox()
        Me.btnRegresar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbCostoMM = New System.Windows.Forms.TextBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.tbUnidad = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbCostoSMO = New System.Windows.Forms.TextBox()
        Me.tbArea = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ITEM:"
        '
        'tbItem
        '
        Me.tbItem.Location = New System.Drawing.Point(72, 21)
        Me.tbItem.Name = "tbItem"
        Me.tbItem.Size = New System.Drawing.Size(176, 20)
        Me.tbItem.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(306, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "ÁREA:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(27, 190)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "DESCRIPCIÓN:"
        '
        'tbDescripcion
        '
        Me.tbDescripcion.Location = New System.Drawing.Point(116, 187)
        Me.tbDescripcion.Multiline = True
        Me.tbDescripcion.Name = "tbDescripcion"
        Me.tbDescripcion.Size = New System.Drawing.Size(411, 83)
        Me.tbDescripcion.TabIndex = 5
        '
        'btnRegresar
        '
        Me.btnRegresar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRegresar.Location = New System.Drawing.Point(382, 301)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(143, 56)
        Me.btnRegresar.TabIndex = 7
        Me.btnRegresar.Text = "Regresar"
        Me.btnRegresar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 66)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "UNIDAD:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 121)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(254, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "COSTO UNITARIO MANO DE OBRA + MATERIAL:"
        '
        'tbCostoMM
        '
        Me.tbCostoMM.Location = New System.Drawing.Point(274, 118)
        Me.tbCostoMM.Name = "tbCostoMM"
        Me.tbCostoMM.Size = New System.Drawing.Size(253, 20)
        Me.tbCostoMM.TabIndex = 3
        '
        'btnAgregar
        '
        Me.btnAgregar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAgregar.Location = New System.Drawing.Point(216, 301)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(143, 56)
        Me.btnAgregar.TabIndex = 6
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'tbUnidad
        '
        Me.tbUnidad.Location = New System.Drawing.Point(72, 63)
        Me.tbUnidad.Name = "tbUnidad"
        Me.tbUnidad.Size = New System.Drawing.Size(176, 20)
        Me.tbUnidad.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(59, 153)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(209, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "COSTO UNITARIO SIN MANO DE OBRA:"
        '
        'tbCostoSMO
        '
        Me.tbCostoSMO.Location = New System.Drawing.Point(274, 150)
        Me.tbCostoSMO.Name = "tbCostoSMO"
        Me.tbCostoSMO.Size = New System.Drawing.Size(251, 20)
        Me.tbCostoSMO.TabIndex = 4
        '
        'tbArea
        '
        Me.tbArea.Location = New System.Drawing.Point(351, 24)
        Me.tbArea.Name = "tbArea"
        Me.tbArea.Size = New System.Drawing.Size(176, 20)
        Me.tbArea.TabIndex = 1
        '
        'AgregarM1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(562, 382)
        Me.Controls.Add(Me.tbCostoSMO)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbUnidad)
        Me.Controls.Add(Me.tbArea)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.tbCostoMM)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnRegresar)
        Me.Controls.Add(Me.tbDescripcion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbItem)
        Me.Controls.Add(Me.Label1)
        Me.Name = "AgregarM1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Agregar Elemento"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents tbItem As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents tbDescripcion As TextBox
    Friend WithEvents btnRegresar As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents tbCostoMM As TextBox
    Friend WithEvents btnAgregar As Button
    Friend WithEvents tbUnidad As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents tbCostoSMO As TextBox
    Friend WithEvents tbArea As TextBox
End Class
