﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class VolumenM3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgVol = New System.Windows.Forms.DataGridView()
        Me.btnRegresar = New System.Windows.Forms.Button()
        Me.dgAdd = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.SIGFOTDataSet = New SIGFOT.SIGFOTDataSet()
        Me.SIGFOTDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbSeccion = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.cbContr = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnCambiar = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        CType(Me.dgVol, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgAdd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SIGFOTDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SIGFOTDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 335)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Volumen"
        '
        'tbBuscar
        '
        Me.tbBuscar.Location = New System.Drawing.Point(94, 54)
        Me.tbBuscar.Name = "tbBuscar"
        Me.tbBuscar.Size = New System.Drawing.Size(260, 20)
        Me.tbBuscar.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Consultar:"
        '
        'dgVol
        '
        Me.dgVol.AllowUserToAddRows = False
        Me.dgVol.AllowUserToDeleteRows = False
        Me.dgVol.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgVol.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgVol.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgVol.Location = New System.Drawing.Point(12, 364)
        Me.dgVol.MultiSelect = False
        Me.dgVol.Name = "dgVol"
        Me.dgVol.RowHeadersVisible = False
        Me.dgVol.Size = New System.Drawing.Size(781, 169)
        Me.dgVol.TabIndex = 4
        '
        'btnRegresar
        '
        Me.btnRegresar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRegresar.Location = New System.Drawing.Point(738, 751)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(143, 56)
        Me.btnRegresar.TabIndex = 17
        Me.btnRegresar.Text = "Regresar"
        Me.btnRegresar.UseVisualStyleBackColor = True
        '
        'dgAdd
        '
        Me.dgAdd.AllowUserToDeleteRows = False
        Me.dgAdd.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAdd.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgAdd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgAdd.Location = New System.Drawing.Point(12, 80)
        Me.dgAdd.MultiSelect = False
        Me.dgAdd.Name = "dgAdd"
        Me.dgAdd.ReadOnly = True
        Me.dgAdd.RowHeadersVisible = False
        Me.dgAdd.Size = New System.Drawing.Size(781, 235)
        Me.dgAdd.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(650, 556)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(143, 56)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Regresar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnModificar.Location = New System.Drawing.Point(484, 556)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(143, 56)
        Me.btnModificar.TabIndex = 5
        Me.btnModificar.Text = "Registrar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'SIGFOTDataSet
        '
        Me.SIGFOTDataSet.DataSetName = "SIGFOTDataSet"
        Me.SIGFOTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SIGFOTDataSetBindingSource
        '
        Me.SIGFOTDataSetBindingSource.DataSource = Me.SIGFOTDataSet
        Me.SIGFOTDataSetBindingSource.Position = 0
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(119, 335)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Sección:"
        '
        'cbSeccion
        '
        Me.cbSeccion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbSeccion.FormattingEnabled = True
        Me.cbSeccion.Location = New System.Drawing.Point(174, 332)
        Me.cbSeccion.Name = "cbSeccion"
        Me.cbSeccion.Size = New System.Drawing.Size(186, 21)
        Me.cbSeccion.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(316, 556)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(143, 56)
        Me.Button2.TabIndex = 7
        Me.Button2.Text = "Limpiar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbContr
        '
        Me.cbContr.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbContr.FormattingEnabled = True
        Me.cbContr.Location = New System.Drawing.Point(94, 20)
        Me.cbContr.Name = "cbContr"
        Me.cbContr.Size = New System.Drawing.Size(260, 21)
        Me.cbContr.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Contratista:"
        '
        'btnCambiar
        '
        Me.btnCambiar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCambiar.Location = New System.Drawing.Point(372, 20)
        Me.btnCambiar.Name = "btnCambiar"
        Me.btnCambiar.Size = New System.Drawing.Size(75, 23)
        Me.btnCambiar.TabIndex = 34
        Me.btnCambiar.Text = "Cambiar"
        Me.btnCambiar.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.Location = New System.Drawing.Point(15, 556)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(143, 56)
        Me.Button4.TabIndex = 8
        Me.Button4.Text = "Imprimir"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'VolumenM3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(823, 636)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.btnCambiar)
        Me.Controls.Add(Me.cbContr)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.cbSeccion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgVol)
        Me.Controls.Add(Me.btnRegresar)
        Me.Controls.Add(Me.dgAdd)
        Me.Name = "VolumenM3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Volumen"
        CType(Me.dgVol, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgAdd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SIGFOTDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SIGFOTDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents tbBuscar As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents dgVol As DataGridView
    Friend WithEvents btnRegresar As Button
    Friend WithEvents dgAdd As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents btnModificar As Button
    Friend WithEvents SIGFOTDataSetBindingSource As BindingSource
    Friend WithEvents SIGFOTDataSet As SIGFOTDataSet
    Friend WithEvents Label3 As Label
    Friend WithEvents cbSeccion As ComboBox
    Friend WithEvents Button2 As Button
    Friend WithEvents cbContr As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btnCambiar As Button
    Friend WithEvents Button4 As Button
End Class
