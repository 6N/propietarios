﻿Imports System.Data.SqlClient
Public Class ParametrosM4
    Dim strCon As String = "Data Source=6N;Initial Catalog=SIGFOT;Integrated Security=True"
    Dim con As New SqlConnection
    Dim command As SqlCommand
    Dim strCommand As String
    Public Last As Integer
    Dim nam As String

    Private Sub ParametrosM4_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "select NombreSeccion from SECCION"
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            con.Open()
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(comando)

            da.Fill(dt)

            dgSeccion.DataSource = dt
            dgSeccion.Columns(0).HeaderText = "Nombre De La Sección"



        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Modulo4.Show()
        Me.Dispose()

    End Sub

    Private Sub dgSeccion_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs)
        If e.Button = MouseButtons.Right Then

            If e.RowIndex < 0 Or e.ColumnIndex < 0 Then Exit Sub
            dgSeccion.CurrentCell = dgSeccion(e.ColumnIndex, e.RowIndex)
            ' Maybe modify the text on your context menu options here?
        End If
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        AgregarM3.Show()
        Me.Hide()
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EliminarToolStripMenuItem.Click
        Dim YesOrNoAnswerToMessageBox As String
        Dim QuestionToMessageBox As String
        Dim row As DataGridViewRow = dgSeccion.CurrentRow

        QuestionToMessageBox = "Esta seguro que desea eliminar el elemento permanentemente?"

        YesOrNoAnswerToMessageBox = MsgBox(QuestionToMessageBox, vbYesNo, "Eliminando")

        If YesOrNoAnswerToMessageBox = vbNo Then

        Else
            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "Select CodSeccion From Seccion Where NombreSeccion='" & CStr(row.Cells(0).Value) & "'"
                '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
                Dim comando As SqlCommand
                Dim rs As SqlDataReader
                con.Open()
                comando = New SqlCommand(sql, con)
                rs = comando.ExecuteReader
                rs.Read()
                Last = rs(0)
                rs.Close()

            Catch ex As Exception
                MessageBox.Show("Error Al Eliminar")
            Finally
                con.Close()
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "DELETE FROM Seccion WHERE codSeccion='" & Last & "'"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                comando.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show("Error Eliminación")
            Finally
                con.Close()
            End Try

            Try
                nam = CStr(row.Cells(0).Value)
                MsgBox(nam)
                con = New SqlConnection(strCon)
                Dim sql As String = "drop table " & nam & " "
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                comando.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show("Error Eliminación")
            Finally
                con.Close()
            End Try



            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select NombreSeccion from Seccion"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)

                da.Fill(dt)

                dgSeccion.DataSource = dt




            Catch ex As Exception
                'MessageBox.Show("Error")
            Finally
                MessageBox.Show("Eliminado!", "Eliminando", MessageBoxButtons.OK, MessageBoxIcon.Information)
                con.Close()
            End Try

        End If

    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuItem.Click
        Me.Hide()
        ModificarM3.Show()

        Dim row As DataGridViewRow = dgSeccion.CurrentRow
        ModificarM3.TextBox1.Text = CStr(row.Cells(0).Value)


        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "Select CodSeccion From Seccion Where NombreSeccion='" & ModificarM3.TextBox1.Text & "'"
            '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
            Dim comando As SqlCommand
            Dim rs As SqlDataReader
            con.Open()
            comando = New SqlCommand(sql, con)
            rs = comando.ExecuteReader
            rs.Read()
            Last = rs(0)
            rs.Close()

        Catch ex As Exception
            MessageBox.Show("Error Al Cargar Datos Para Modificar")
        Finally
            con.Close()
        End Try
    End Sub
End Class