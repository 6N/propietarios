﻿Imports System.Data.SqlClient

Public Class AgregarM2
    Dim strCon As String = "Data Source=6N;Initial Catalog=SIGFOT;Integrated Security=True"
    Dim con As New SqlConnection
    Dim command As SqlCommand
    Dim strCommand As String
    Dim Last As Integer
    Dim dupl As Integer
    Dim ced As Integer
    Dim em As Integer
    Dim tel As Integer
    Dim t1, t2, t3, t4, t5, t6 As String
    Private Sub btnRegresar_Click(sender As Object, e As EventArgs) Handles btnRegresar.Click
        Modulo2.Show()
        Me.Dispose()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If tbNombre.Text = "" Then
            t1 = "" & vbCrLf & "- Nombre"

        Else
            t1 = ""
        End If
        If tbApellido.Text = "" Then
            t2 = "" & vbCrLf & "- Apellido"
        Else
            t2 = ""

        End If

        If em = 0 Then
            t3 = "" & vbCrLf & "- Correo Electrónico"
        Else
            t3 = ""
        End If

        If ced = 0 Then
            t4 = "" & vbCrLf & "- RUC"
        Else
            t4 = ""
        End If

        If tel = 0 Then
            t5 = "" & vbCrLf & "- Celular"
        Else
            t5 = ""
        End If


        If tbDireccion.Text = "" Then
            t6 = "" & vbCrLf & "- Dirección Domiciliaria"
        Else
            t6 = ""
        End If

        If t1 = "" And t2 = "" And t3 = "" And t4 = "" And t5 = "" And t6 = "" Then



            If Modulo2.dgModulo1.Rows.Count = 0 Then

            Else
                Try
                    con = New SqlConnection(strCon)
                    Dim sql As String = "Select MAX(CodContratista) From Contratista"
                    Dim comando As SqlCommand
                    Dim rs As SqlDataReader
                    con.Open()
                    comando = New SqlCommand(sql, con)
                    rs = comando.ExecuteReader
                    rs.Read()
                    Last = rs(0) + 1
                    rs.Close()
                Catch ex As Exception
                    MessageBox.Show("Error En Insersión")
                Finally
                    con.Close()
                End Try
            End If


            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "insert into Contratista (CodContratista,NombreContr,ApellidoContr,DireccionContr,TelefonoContr,
                                     Correoecontr,Ruccontr) values(
                                               '" & Last & "','" & tbNombre.Text & "','" & tbApellido.Text & "','" & tbDireccion.Text & "'
                                              ,'" & tbTelefono.Text & "','" & tbCorreo.Text & "','" & tbRuc.Text & "')"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                comando.ExecuteNonQuery()

            Catch ex As Exception
                MessageBox.Show("Error En Insercción")
            Finally
                con.Close()
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "Select Count(RucContr) From Contratista group by RucContr having count(RucContr) > 1"
                Dim comando As SqlCommand
                Dim rs As SqlDataReader
                con.Open()
                comando = New SqlCommand(sql, con)
                rs = comando.ExecuteReader
                If rs.Read() Then
                    dupl = rs(0)
                Else
                    dupl = 0
                End If

                rs.Close()
            Catch ex As Exception
                MessageBox.Show("Error Chequeo Dobles")
            Finally
                con.Close()
            End Try

            If dupl > 1 Then
                MsgBox("Ya existe un contratista con esos datos! Verifique!")
                Try
                    con = New SqlConnection(strCon)
                    Dim sql As String = "delete from CONTRATISTA where CodContratista=" & Last & ""
                    Dim comando As SqlCommand
                    Dim rs As SqlDataReader
                    con.Open()
                    comando = New SqlCommand(sql, con)
                    rs = comando.ExecuteReader
                    If rs.Read() Then
                        dupl = rs(0)
                    Else
                        dupl = 0
                    End If

                    rs.Close()
                Catch ex As Exception
                    MessageBox.Show("Error Elimnar Doble")
                Finally
                    con.Close()
                End Try


            Else
                Try
                    con = New SqlConnection(strCon)
                    Dim sql As String = "select * from CONTRATISTA"
                    Dim comando As SqlCommand
                    comando = New SqlCommand(sql, con)
                    con.Open()
                    Dim dt As New DataTable()
                    Dim da As New SqlDataAdapter(comando)

                    da.Fill(dt)

                    Modulo2.dgModulo1.DataSource = dt


                Catch ex As Exception
                    MessageBox.Show("Error De Lectura")
                Finally
                    MessageBox.Show("Agregado Éxitosamente!", "Agregando", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Dispose()
                    Modulo2.Show()
                    con.Close()
                End Try

            End If




        Else
            MessageBox.Show("Verifique la información de los siguientes campos: : " & t1 & "" & t2 & "" & t5 & "" & t4 & "" & t3 & "" & t6 & "", "Campos Vacios", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

        End If




    End Sub

    Private Sub tbNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbNombre.KeyPress
        SoloTexto(e)
        'Me.tbNombre.Text = Trim(Replace(Me.tbNombre.Text, "  ", " "))
        'tbNombre.Select(tbNombre.Text.Length, 0)
    End Sub

    Private Sub tbApellido_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbApellido.KeyPress
        SoloTexto(e)
        'Me.tbApellido.Text = Trim(Replace(Me.tbApellido.Text, "  ", " "))
        'tbApellido.Select(tbApellido.Text.Length, 0)
    End Sub

    Private Sub tbTelefono_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbTelefono.KeyPress
        SoloNumeros(tbTelefono, e)

    End Sub

    Private Sub tbTelefono_Leave(sender As Object, e As EventArgs) Handles tbTelefono.Leave
        If tbTelefono.TextLength < 10 Then
            MsgBox("El número de dígitos ingresados en el campo teléfono no es el correcto. Verifique!", vbExclamation, "Advertencia!")
            tel = 0
        Else
            tel = 1
        End If

    End Sub

    Private Sub AgregarM2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tbTelefono.MaxLength = 10
        tbRuc.MaxLength = 13
        ced = 0


    End Sub

    Private Sub tbRuc_Leave(sender As Object, e As EventArgs) Handles tbRuc.Leave
        If tbRuc.TextLength < 13 Then
            MsgBox("El número de dígitos ingresados en el campo RUC no es el correcto. Verifique!", vbExclamation, "Advertencia")
            ced = 0
        Else
            If tbRuc.TextLength = 13 Then
                If VerificaCedula(Strings.Left(tbRuc.Text, 10)) = False Then
                    ced = 0

                Else
                    ced = 1
                End If
            Else
                MsgBox("El número de dígitos ingresados en el campo RUC no es el correcto. Verifique!", vbExclamation, "Advertencia!")

            End If
        End If


    End Sub

    Private Sub tbRuc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbRuc.KeyPress
        SoloNumeros(tbRuc, e)
    End Sub

    Private Sub tbCorreo_Leave(sender As Object, e As EventArgs) Handles tbCorreo.Leave
        If ValidarEmail(tbCorreo.Text) = True Then
            em = 1
        Else
            em = 0
        End If
    End Sub

    Private Sub AgregarM2_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Modulo2.Show()
        Me.Dispose()
    End Sub
End Class