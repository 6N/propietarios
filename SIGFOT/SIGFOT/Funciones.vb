﻿Option Explicit On
Imports System.IO
Imports System.Text.RegularExpressions

Module Funciones
    Public ruta As String = "C:\Users\ASUS\Desktop\Proyecto Propietarios"
    Public a, b As String
    'leemos el archivo con IO.StreamReader y pasamos la informacion a las varianles info y info1
    Public Sub cargar()
        Dim intLines As Integer = 0
        Dim sr As New StreamReader(ruta + "\archivo.txt", OpenMode.Input)
        Dim info, info1 As New TextBox
        Do While sr.Peek() >= 0
            info.Text += sr.ReadLine()
            info1.Text += sr.ReadLine()
            a = info.Text
            b = info1.Text
            intLines += 1
        Loop
        'siempre se debe cerrar el archivo abierto.. depende del caso!
        sr.Close()
        Exit Sub
    End Sub

    Public Sub SoloTexto(e As KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If


    End Sub
    Public Sub TextoyNumeros(e As KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If


    End Sub
    Public Sub NumerosyDecimal(ByVal CajaTexto As TextBox, e As KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",") Then
            e.Handled = True
        ElseIf e.KeyChar = "," Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub
    Public Sub SoloNumeros(ByVal CajaTexto As TextBox, e As KeyPressEventArgs)
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub
    Public Function VerificaCedula(Cedula As String) As Boolean
        VerificaCedula = True

        If Cedula = "0000000000" Then
            VerificaCedula = False
        End If

        If Len(Trim(Cedula)) <> 10 Then
            VerificaCedula = False
        End If

        If Val(Mid(Cedula, 1, 2)) > 25 Then
            VerificaCedula = False
        End If

        If Val(Mid(Cedula, 3, 1)) > 5 Then
            VerificaCedula = False
        End If

        If VerificaCedula = False Then
            MsgBox("RUC Incorrecto! ", vbInformation)

        Else
            Dim Total As Integer
            Dim Cifra As Integer
            Total = 0

            For x = 1 To 9

                If (x Mod 2) = 0 Then
                    Cifra = Val(Mid(Cedula, x, 1))
                Else
                    Cifra = Val(Mid(Cedula, x, 1)) * 2
                    If Cifra > 9 Then
                        Cifra = Cifra - 9
                    End If
                End If
                Total = Total + Cifra
            Next

            Cifra = Total Mod 10

            If Cifra > 0 Then
                Cifra = 10 - Cifra
            End If

            If Cifra = Val(Mid(Cedula, 10, 1)) Then
                VerificaCedula = True
            Else
                MsgBox("Número de RUC incorrecto, verifique por favor!", vbInformation, "RUC Incorrecto")
                VerificaCedula = False
            End If

        End If


    End Function
    Public Function ValidarEmail(ByVal Email As String) As Boolean

        Dim strTmp As String
        Dim n As Long
        Dim sEXT As String


        ValidarEmail = True

        sEXT = Email

        Do While InStr(1, sEXT, ".") <> 0
            sEXT = Right(sEXT, Len(sEXT) - InStr(1, sEXT, "."))
        Loop

        If Email = "" Then
            ValidarEmail = False
            MsgBox("No se indicó ninguna dirección de mail para verificar!")
        ElseIf InStr(1, Email, "@") = 0 Then
            ValidarEmail = False
            MsgBox("La dirección de email no contiene el signo @")
        ElseIf InStr(1, Email, "@") = 1 Then
            ValidarEmail = False
            MsgBox("El @ No puede estar al principio")
        ElseIf InStr(1, Email, "@") = Len(Email) Then
            ValidarEmail = False
            MsgBox("El @ no puede estar al final de la dirección")
        ElseIf EXTisOK(sEXT) = False Then
            ValidarEmail = False
            MsgBox("La dirección no tiene un dominio válido! ")
        ElseIf Len(Email) < 6 Then
            ValidarEmail = False
            MsgBox("La dirección no puede ser menor a 6 caracteres.")
        ElseIf Left(email, 1) = "@" Then
            ValidarEmail = False
            MsgBox("La dirección de email ingresada es incorrecta!")
        ElseIf Right(email, 1) = "@" Then
            ValidarEmail = False
            MsgBox("La dirección de email ingresada es incorrecta!")
        ElseIf InStr(1, email, "@") = False Then
            ValidarEmail = False
            MsgBox("La dirección de email ingresada es incorrecta!")
        ElseIf InStr(1, email, ".") = False Then
            ValidarEmail = False
            MsgBox("La dirección de email ingresada es incorrecta!")
        ElseIf Right(email, 1) = "." Then
            ValidarEmail = False
            MsgBox("La dirección de email ingresada es incorrecta!")
        ElseIf Left(email, 1) = "." Then
            ValidarEmail = False
            MsgBox("La dirección de email ingresada es incorrecta!")
        ElseIf ContarCaracteres(Email, "@") > 1 Then
            ValidarEmail = False
            MsgBox("La dirección de email ingresada es incorrecta!")
        End If
        strTmp = Email
        Do While InStr(1, strTmp, "@") <> 0
            n = 1
            strTmp = Right(strTmp, Len(strTmp) - InStr(1, strTmp, "@"))
        Loop
        If n > 1 Then
            ValidarEmail = False
            MsgBox("Solo puede haber un @ en la dirección de email")
        End If

        Dim pos As Integer

        pos = InStr(1, Email, "@")

        If Mid(Email, pos + 1, 1) = "." Then
            ValidarEmail = False
            MsgBox("El punto no puede estar seguido del símbolo @")
        End If


    End Function


    Public Function EXTisOK(ByVal sEXT As String) As Boolean
        Dim EXT As String
        EXTisOK = False
        EXT = ""
        If Left(sEXT, 1) <> "." Then sEXT = "." & sEXT
        sEXT = UCase(sEXT) 'just to avoid errors
        EXT = EXT & ".COM.EDU.GOV.NET.BIZ.ORG.TV"
        EXT = EXT & ".AF.AL.DZ.As.AD.AO.AI.AQ.AG.AP.AR.AM.AW.AU.AT.AZ.BS.BH.BD.BB.BY"
        EXT = EXT & ".BE.BZ.BJ.BM.BT.BO.BA.BW.BV.BR.IO.BN.BG.BF.MM.BI.KH.CM.CA.CV.KY"
        EXT = EXT & ".CF.TD.CL.CN.CX.CC.CO.KM.CG.CD.CK.CR.CI.HR.CU.CY.CZ.DK.DJ.DM.DO"
        EXT = EXT & ".TP.EC.EG.SV.GQ.ER.EE.ET.FK.FO.FJ.FI.CS.SU.FR.FX.GF.PF.TF.GA.GM.GE.DE"
        EXT = EXT & ".GH.GI.GB.GR.GL.GD.GP.GU.GT.GN.GW.GY.HT.HM.HN.HK.HU.IS.IN.ID.IR.IQ"
        EXT = EXT & ".IE.IL.IT.JM.JP.JO.KZ.KE.KI.KW.KG.LA.LV.LB.LS.LR.LY.LI.LT.LU.MO.MK.MG"
        EXT = EXT & ".MW.MY.MV.ML.MT.MH.MQ.MR.MU.YT.MX.FM.MD.MC.MN.MS.MA.MZ.NA"
        EXT = EXT & ".NR.NP.NL.AN.NT.NC.NZ.NI.NE.NG.NU.NF.KP.MP.NO.OM.PK.PW.PA.PG.PY"
        EXT = EXT & ".PE.PH.PN.PL.PT.PR.QA.RE.RO.RU.RW.GS.SH.KN.LC.PM.ST.VC.SM.SA.SN.SC"
        EXT = EXT & ".SL.SG.SK.SI.SB.SO.ZA.KR.ES.LK.SD.SR.SJ.SZ.SE.CH.SY.TJ.TW.TZ.TH.TG.TK"
        EXT = EXT & ".TO.TT.TN.TR.TM.TC.TV.UG.UA.AE.UK.US.UY.UM.UZ.VU.VA.VE.VN.VG.VI"
        EXT = EXT & ".WF.WS.EH.YE.YU.ZR.ZM.ZW"
        EXT = UCase(EXT) 'just to avoid errors
        If InStr(1, EXT, sEXT, 0) <> 0 Then
            EXTisOK = True
        End If
    End Function

    Private Function ContarCaracteres(ByVal cadena As String, ByVal caracter As Char) As Integer

        Dim n As Integer
        Dim contador As Integer = 0

        For n = 0 To Len(cadena) - 1
            If cadena.Chars(n) = caracter Then
                contador = contador + 1
            End If
        Next n
        Return contador

    End Function

    Public Function Salir(e As FormClosingEventArgs)
        Dim YesOrNoAnswerToMessageBox As String
        Dim QuestionToMessageBox As String


        QuestionToMessageBox = "¿Está seguro que desea cerrar el sistema?"

        YesOrNoAnswerToMessageBox = MsgBox(QuestionToMessageBox, vbYesNo, "Cerrando")

        If YesOrNoAnswerToMessageBox = vbNo Then
            e.Cancel = True
        Else
            Application.ExitThread()
        End If
        Return 0
    End Function

End Module
