﻿Imports System.Data.SqlClient
Public Class ModificarM1
    Dim strCon As String = "Data Source=6N;Initial Catalog=SIGFOT;Integrated Security=True"
    Dim con As New SqlConnection
    Dim command As SqlCommand
    Dim strCommand As String
    Dim Last As Integer
    Dim dupl As Integer
    Dim t1, t2, t3, t4, t5, t6 As String

    Private Sub btnRegresar_Click(sender As Object, e As EventArgs) Handles btnRegresar.Click
        Modulo1.Show()
        Me.Dispose()
        Modulo1.tbBuscar.Text = ""
    End Sub
    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click

        If tbItem.Text = "" Then
            t1 = "" & vbCrLf & "- Item"

        Else
            t1 = ""
        End If
        If tbArea.Text = "" Then
            t2 = "" & vbCrLf & "- Área"
        Else
            t2 = ""

        End If

        If tbUnidad.Text = "" Then
            t3 = "" & vbCrLf & "- Unidad"
        Else
            t3 = ""
        End If

        If tbCostoMM.Text = "" Then
            t4 = "" & vbCrLf & "- Costo Unitario Mano De Obra + Material"
        Else
            t4 = ""
        End If

        If tbCostoSMO.Text = "" Then
            t5 = "" & vbCrLf & "- Costo Unitario Sin Mano De Obra"
        Else
            t5 = ""
        End If


        If tbDescripcion.Text = "" Then
            t6 = "" & vbCrLf & "- Descripción"
        Else
            t6 = ""
        End If
        If t1 = "" And t2 = "" And t3 = "" And t4 = "" And t5 = "" And t6 = "" Then

            Dim YesOrNoAnswerToMessageBox As String
            Dim QuestionToMessageBox As String


            QuestionToMessageBox = "Está seguro que desea modificar esta información permanentemente?"

            YesOrNoAnswerToMessageBox = MsgBox(QuestionToMessageBox, vbYesNo, "Agregando")

            If YesOrNoAnswerToMessageBox = vbNo Then

            Else
                Try
                    con = New SqlConnection(strCon)

                    Dim sql As String = "UPDATE Elemento SET ITEM ='" & tbItem.Text & "', AREA ='" & tbArea.Text & "',
                    DESCRIPCION ='" & tbDescripcion.Text & "',  UNIDAD ='" & tbUnidad.Text & "', 
                    CUMM =" & Str(tbCostoMM.Text) & ",  CUSMO =" & Str(tbCostoSMO.Text) & " where CodElemento='" & Modulo1.Last & "'"
                    Dim comando As SqlCommand
                    comando = New SqlCommand(sql, con)
                    con.Open()

                    comando.ExecuteNonQuery()

                Catch ex As Exception
                    MessageBox.Show("Error Al Modificar")
                Finally
                    con.Close()
                End Try

                Try
                    con = New SqlConnection(strCon)
                    Dim sql As String = "select * from ELEMENTO"
                    Dim comando As SqlCommand
                    comando = New SqlCommand(sql, con)
                    con.Open()
                    Dim dt As New DataTable()
                    Dim da As New SqlDataAdapter(comando)

                    da.Fill(dt)

                    Modulo1.dgModulo1.DataSource = dt


                Catch ex As Exception
                    MessageBox.Show("Error Carga De Datos")
                Finally
                    MessageBox.Show("Modificado!", "Modificado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Dispose()
                    Modulo1.Show()
                    con.Close()
                End Try
            End If

        Else

            MessageBox.Show("Verifique la información de los siguientes campos: " & t1 & "" & t2 & "" & t3 & "" & t4 & "" & t5 & "" & t6 & "", "Campos Vacios", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)


        End If





    End Sub

    Private Sub tbCostoMM_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbCostoMM.KeyPress
        NumerosyDecimal(tbCostoMM, e)
    End Sub

    Private Sub tbCostoSMO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbCostoSMO.KeyPress
        NumerosyDecimal(tbCostoSMO, e)
    End Sub

    Private Sub tbItem_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbItem.KeyPress
        TextoyNumeros(e)
        Me.tbItem.Text = Trim(Replace(Me.tbItem.Text, "  ", " "))
        tbItem.Select(tbItem.Text.Length, 0)
    End Sub

    Private Sub tbArea_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbArea.KeyPress
        SoloTexto(e)
        Me.tbArea.Text = Trim(Replace(Me.tbArea.Text, "  ", " "))
        tbArea.Select(tbArea.Text.Length, 0)
    End Sub

    Private Sub tbUnidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbUnidad.KeyPress
        TextoyNumeros(e)

        Me.tbUnidad.Text = Trim(Replace(Me.tbUnidad.Text, "  ", " "))
        tbUnidad.Select(tbUnidad.Text.Length, 0)
    End Sub

    Private Sub AgregarM1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Modulo1.Show()
        Me.Dispose()
    End Sub
End Class