﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Modulo2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.tbBuscar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgModulo1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ModificarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VisualizarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CONTRATISTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SIGFOTDataSet1 = New SIGFOT.SIGFOTDataSet()
        Me.CONTRATISTATableAdapter1 = New SIGFOT.SIGFOTDataSetTableAdapters.CONTRATISTATableAdapter()
        CType(Me.dgModulo1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.CONTRATISTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SIGFOTDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(734, 411)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(143, 56)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Regresar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAgregar.Location = New System.Drawing.Point(387, 11)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(143, 56)
        Me.btnAgregar.TabIndex = 1
        Me.btnAgregar.Text = "Registrar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'tbBuscar
        '
        Me.tbBuscar.Location = New System.Drawing.Point(73, 27)
        Me.tbBuscar.Name = "tbBuscar"
        Me.tbBuscar.Size = New System.Drawing.Size(225, 20)
        Me.tbBuscar.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Consultar:"
        '
        'dgModulo1
        '
        Me.dgModulo1.AllowUserToAddRows = False
        Me.dgModulo1.AllowUserToDeleteRows = False
        Me.dgModulo1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgModulo1.AutoGenerateColumns = False
        Me.dgModulo1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgModulo1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgModulo1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.dgModulo1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgModulo1.DataSource = Me.CONTRATISTABindingSource
        Me.dgModulo1.Location = New System.Drawing.Point(11, 92)
        Me.dgModulo1.MultiSelect = False
        Me.dgModulo1.Name = "dgModulo1"
        Me.dgModulo1.ReadOnly = True
        Me.dgModulo1.RowHeadersVisible = False
        Me.dgModulo1.Size = New System.Drawing.Size(866, 300)
        Me.dgModulo1.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "NOMBRECONTR"
        Me.DataGridViewTextBoxColumn1.HeaderText = "NOMBRECONTR"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "APELLIDOCONTR"
        Me.DataGridViewTextBoxColumn2.HeaderText = "APELLIDOCONTR"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "DIRECCIONCONTR"
        Me.DataGridViewTextBoxColumn3.HeaderText = "DIRECCIONCONTR"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "TELEFONOCONTR"
        Me.DataGridViewTextBoxColumn4.HeaderText = "TELEFONOCONTR"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "CORREOECONTR"
        Me.DataGridViewTextBoxColumn5.HeaderText = "CORREOECONTR"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "RUCCONTR"
        Me.DataGridViewTextBoxColumn6.HeaderText = "RUCCONTR"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarToolStripMenuItem, Me.VisualizarToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(126, 48)
        '
        'ModificarToolStripMenuItem
        '
        Me.ModificarToolStripMenuItem.Name = "ModificarToolStripMenuItem"
        Me.ModificarToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.ModificarToolStripMenuItem.Text = "Modificar"
        '
        'VisualizarToolStripMenuItem
        '
        Me.VisualizarToolStripMenuItem.Name = "VisualizarToolStripMenuItem"
        Me.VisualizarToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.VisualizarToolStripMenuItem.Text = "Visualizar"
        '
        'CONTRATISTABindingSource
        '
        Me.CONTRATISTABindingSource.DataMember = "CONTRATISTA"
        Me.CONTRATISTABindingSource.DataSource = Me.SIGFOTDataSet1
        '
        'SIGFOTDataSet1
        '
        Me.SIGFOTDataSet1.DataSetName = "SIGFOTDataSet"
        Me.SIGFOTDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONTRATISTATableAdapter1
        '
        Me.CONTRATISTATableAdapter1.ClearBeforeFill = True
        '
        'Modulo2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(895, 479)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.tbBuscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgModulo1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Modulo2"
        Me.Text = "Contratista"
        CType(Me.dgModulo1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.CONTRATISTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SIGFOTDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents btnAgregar As Button
    Friend WithEvents tbBuscar As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents dgModulo1 As DataGridView
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ModificarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VisualizarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SIGFOTDataSet As SIGFOTDataSet
    Friend WithEvents CONTRATISTATableAdapter As SIGFOTDataSetTableAdapters.CONTRATISTATableAdapter
    Friend WithEvents NOMBRECONTRDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents APELLIDOCONTRDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DIRECCIONCONTRDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TELEFONOCONTRDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CORREOECONTRDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RUCCONTRDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SIGFOTDataSet1 As SIGFOTDataSet
    Friend WithEvents CONTRATISTABindingSource As BindingSource
    Friend WithEvents CONTRATISTATableAdapter1 As SIGFOTDataSetTableAdapters.CONTRATISTATableAdapter
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
End Class
