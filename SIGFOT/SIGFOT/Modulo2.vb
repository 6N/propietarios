﻿Imports System.Data.SqlClient
Public Class Modulo2
    Dim strCon As String = "Data Source=6N;Initial Catalog=SIGFOT;Integrated Security=True"
    Dim con As New SqlConnection
    Dim command As SqlCommand
    Dim strCommand As String
    Public Last As Integer
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MenuPrincipal.Show()
        Me.Dispose()
    End Sub


    Private Sub Modulo2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'SIGFOTDataSet1.CONTRATISTA' Puede moverla o quitarla según sea necesario.
        Me.CONTRATISTATableAdapter1.Fill(Me.SIGFOTDataSet1.CONTRATISTA)


        dgModulo1.Columns(0).Width = 80
        dgModulo1.Columns(1).Width = 80
        dgModulo1.Columns(2).Width = 300

        dgModulo1.Columns(0).HeaderText = "NOMBRE"
        dgModulo1.Columns(1).HeaderText = "APELLIDO"
        dgModulo1.Columns(2).HeaderText = "DIRECCIÓN DOMICILIARIA"
        dgModulo1.Columns(3).HeaderText = "CELULAR"
        dgModulo1.Columns(4).HeaderText = "CORREO ELECTRÓNICO"
        dgModulo1.Columns(5).HeaderText = "RUC"


        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "select * from CONTRATISTA"
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            con.Open()
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(comando)

            da.Fill(dt)

            dgModulo1.DataSource = dt




        Catch ex As Exception
            MessageBox.Show("Error Presentación")
        Finally
            con.Close()
        End Try
    End Sub

    Private Sub tbBuscar_TextChanged(sender As Object, e As EventArgs) Handles tbBuscar.TextChanged
        If tbBuscar.Text = "" Then
            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select * from CONTRATISTA"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)

                da.Fill(dt)

                dgModulo1.DataSource = dt




            Catch ex As Exception
                MessageBox.Show("Error Presentación")
            Finally
                con.Close()
            End Try

        Else

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select * from CONTRATISTA where NOMBRECONTR like '%" & tbBuscar.Text & "%' 
                or APELLIDOCONTR like '%" & tbBuscar.Text & "%'
                or DIRECCIONCONTR like '%" & tbBuscar.Text & "%'
                or TELEFONOCONTR like '%" & tbBuscar.Text & "%'
                or CORREOECONTR like '%" & tbBuscar.Text & "%'
                or RUCCONTR like '%" & tbBuscar.Text & "%'"


                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)
                da.Fill(dt)
                dgModulo1.DataSource = dt




            Catch ex As Exception
                MessageBox.Show("Error Base Contratista")
            Finally
                con.Close()
            End Try

        End If
    End Sub
    Private Sub dgModulo1_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgModulo1.CellMouseDown
        If e.Button = MouseButtons.Right Then

            If e.RowIndex < 0 Or e.ColumnIndex < 0 Then Exit Sub
            dgModulo1.CurrentCell = dgModulo1(e.ColumnIndex, e.RowIndex)
            ' Maybe modify the text on your context menu options here?
        End If
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Me.Hide()
        AgregarM2.Show()
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuItem.Click
        Me.Hide()
        ModificarM2.Show()

        Dim row As DataGridViewRow = dgModulo1.CurrentRow
        ModificarM2.tbNombre.Text = CStr(row.Cells(0).Value)
        ModificarM2.tbApellido.Text = CStr(row.Cells(1).Value)
        ModificarM2.tbDireccion.Text = CStr(row.Cells(2).Value)
        ModificarM2.tbTelefono.Text = CStr(row.Cells(3).Value)
        ModificarM2.tbCorreo.Text = CStr(row.Cells(4).Value)
        ModificarM2.tbRuc.Text = CStr(row.Cells(5).Value)

        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "Select CodContratista From Contratista Where NombreContr='" & ModificarM2.tbNombre.Text & "'"
            '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
            Dim comando As SqlCommand
            Dim rs As SqlDataReader
            con.Open()
            comando = New SqlCommand(sql, con)
            rs = comando.ExecuteReader
            rs.Read()
            Last = rs(0)
            rs.Close()

        Catch ex As Exception
            MessageBox.Show("Error Carga Datos Modificar")
        Finally
            con.Close()
        End Try

    End Sub
    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) 
        Dim YesOrNoAnswerToMessageBox As String
        Dim QuestionToMessageBox As String
        Dim row As DataGridViewRow = dgModulo1.CurrentRow

        QuestionToMessageBox = "Esta seguro que desea eliminar esta informacion permanentemente?"

        YesOrNoAnswerToMessageBox = MsgBox(QuestionToMessageBox, vbYesNo, "Eliminando")

        If YesOrNoAnswerToMessageBox = vbNo Then

        Else

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "Select CodContratista From Contratista Where ruccontr='" & CStr(row.Cells(5).Value) & "'"
                '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
                Dim comando As SqlCommand
                Dim rs As SqlDataReader
                con.Open()
                comando = New SqlCommand(sql, con)
                rs = comando.ExecuteReader
                rs.Read()
                Last = rs(0)
                rs.Close()

            Catch ex As Exception
                MessageBox.Show("Error Eliminar Contratista")
            Finally
                con.Close()
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "DELETE FROM contratista WHERE codcontratista='" & Last & "'"

                '"Select CodElemento From Elementos Where Item='" & ModificarM1.tbItem.Text & "'"
                '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                comando.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show("Error Eliminación")
            Finally
                con.Close()
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select * from contratista"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)

                da.Fill(dt)

                dgModulo1.DataSource = dt




            Catch ex As Exception
                'MessageBox.Show("Error")
            Finally
                tbBuscar.Text = ""
                MessageBox.Show("Eliminado!", "Eliminando", MessageBoxButtons.OK, MessageBoxIcon.Information)
                con.Close()
            End Try

        End If


    End Sub
    Private Sub VisualizarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VisualizarToolStripMenuItem.Click
        Me.Hide()
        VisualizarM2.Show()

        Dim row As DataGridViewRow = dgModulo1.CurrentRow
        VisualizarM2.tbNombre.Text = CStr(row.Cells(0).Value)
        VisualizarM2.tbApellido.Text = CStr(row.Cells(1).Value)
        VisualizarM2.tbDireccion.Text = CStr(row.Cells(2).Value)
        VisualizarM2.tbTelefono.Text = CStr(row.Cells(3).Value)
        VisualizarM2.tbCorreo.Text = CStr(row.Cells(4).Value)
        VisualizarM2.tbRuc.Text = CStr(row.Cells(5).Value)

    End Sub

    Private Sub Modulo2_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Salir(e)
    End Sub
End Class