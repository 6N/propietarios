﻿Imports System.Data.SqlClient
Imports Microsoft.Office.Interop
Public Class VolumenM3
    Dim strCon As String = "Data Source=6N;Initial Catalog=SIGFOT;Integrated Security=True"
    Dim con As New SqlConnection
    Dim command As SqlCommand
    Dim strCommand As String
    Dim secc As String
    Dim one As Boolean = False
    Dim two As Boolean = False
    Dim hm As Integer

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MenuPrincipal.Show()
        Me.Dispose()
    End Sub

    Private Sub VolumenM3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btnCambiar.Visible = False
        one = False
        two = False
        If cbContr.Text = "" Then
            btnModificar.Enabled = False
            Button4.Enabled = False
            tbBuscar.Enabled = False
            cbSeccion.Enabled = False
            Button2.Enabled = False
        End If




        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "select NombreSeccion from Seccion"
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            Dim rs As SqlDataReader
            con.Open()
            comando = New SqlCommand(sql, con)
            rs = comando.ExecuteReader

            While rs.Read
                cbSeccion.Items.Add(rs.Item("NombreSeccion"))
            End While

            rs.Close()


        Catch ex As Exception
            MessageBox.Show("Error Seccion")
        Finally
            con.Close()
        End Try

        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "select NombreContr,ApellidoContr from Contratista"
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            Dim rs As SqlDataReader
            con.Open()
            comando = New SqlCommand(sql, con)
            rs = comando.ExecuteReader

            While rs.Read
                cbContr.Items.Add(rs.Item("NombreContr") + " " + rs.Item("ApellidoContr"))
            End While

            rs.Close()


        Catch ex As Exception
            MessageBox.Show("Error Seccion")
        Finally
            con.Close()
        End Try


    End Sub

    Private Sub dgAdd_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgAdd.CellClick
        If dgAdd.CurrentCell.GetType.ToString Like "*Button*" Then
            MsgBox(dgAdd.CurrentRow.Cells(0).Value())

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "INSERT INTO " & secc & " (Seccion,Contratista,ItemV,Detalle,UnidadV,PrecioMM,
                PrecioSMO) values (
                '" & cbSeccion.Text & "',  '" & cbContr.Text & "',  '" & dgAdd.CurrentRow.Cells(0).Value() & "', 
                '" & dgAdd.CurrentRow.Cells(2).Value() & "',  '" & dgAdd.CurrentRow.Cells(3).Value() & "'
                ,  '" & dgAdd.CurrentRow.Cells(4).Value() & "',  '" & dgAdd.CurrentRow.Cells(5).Value() & "'
                )"

                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                comando.ExecuteNonQuery()

                con.Close()

            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                MessageBox.Show("Agregado Éxitosamente!", "Agregando", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select ItemV,Detalle,UnidadV,PrecioMM,
                PrecioSMO,SN,Contratado,Replanteado,Construido from " & secc & ""
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)

                da.Fill(dt)

                dgVol.DataSource = dt


            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                con.Close()
            End Try


        End If




        'Dim row As New DataGridViewRow
        'row = dgAdd.CurrentRow
        '
    End Sub
    Private Sub dgVol_Click(sender As Object, e As EventArgs) Handles dgVol.Click
        Dim row As New DataGridViewRow
        row = dgVol.CurrentRow
        If dgVol.Rows.Count > 0 Then
            If dgVol.CurrentCell.GetType.ToString Like "*Button*" Then
                Try
                    con = New SqlConnection(strCon)
                    Dim sql As String = "DELETE FROM " & secc & " WHERE Itemv= '" & dgVol.CurrentRow.Cells("Itemv").Value() & "'"

                    '"Select CodElemento From Elementos Where Item='" & ModificarM1.tbItem.Text & "'"
                    '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
                    Dim comando As SqlCommand
                    comando = New SqlCommand(sql, con)
                    con.Open()
                    comando.ExecuteNonQuery()
                Catch ex As Exception
                    MessageBox.Show("Error Eliminación")
                Finally
                    con.Close()
                End Try

                Try
                    con = New SqlConnection(strCon)
                    Dim sql As String = "select ItemV,Detalle,UnidadV,PrecioMM,
                PrecioSMO,SN,Contratado,Replanteado,Construido from " & secc & ""
                    Dim comando As SqlCommand
                    comando = New SqlCommand(sql, con)
                    con.Open()
                    Dim dt As New DataTable()
                    Dim da As New SqlDataAdapter(comando)

                    da.Fill(dt)

                    dgVol.DataSource = dt


                Catch ex As Exception
                    MessageBox.Show("Error")
                Finally
                    con.Close()
                End Try

            End If
        End If

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles tbBuscar.TextChanged
        If tbBuscar.Text = "" Then
            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select ITEM,AREA,DESCRIPCION,UNIDAD,CUMM,CUSMO from ELEMENTO"
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)

                da.Fill(dt)

                dgAdd.DataSource = dt

            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                con.Close()
            End Try

        Else

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select ITEM,AREA,DESCRIPCION,UNIDAD,CUMM,CUSMO from ELEMENTO
                where item like '%" & tbBuscar.Text & "%' 
                or area like '%" & tbBuscar.Text & "%'
                or descripcion like '%" & tbBuscar.Text & "%'
                or unidad like '%" & tbBuscar.Text & "%'
                or cumm like '%" & tbBuscar.Text & "%'
                Or cusmo Like '%" & tbBuscar.Text & "%'"

                'ITEM, DESCRIPCION, UNIDAD
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(comando)
                da.Fill(dt)
                dgAdd.DataSource = dt


            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                con.Close()
            End Try

        End If
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Dim YesOrNoAnswerToMessageBox As String
        Dim QuestionToMessageBox As String

        QuestionToMessageBox = "Está seguro que desea agregar a la sección seleccionada esta información?"

        YesOrNoAnswerToMessageBox = MsgBox(QuestionToMessageBox, vbYesNo, "Eliminando")

        If YesOrNoAnswerToMessageBox = vbNo Then

        Else
            secc = cbSeccion.Text.Replace(" "c, String.Empty)
            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "DELETE FROM " & secc & " "
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                comando.ExecuteNonQuery()


            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                con.Close()
            End Try


            Try

                For i As Integer = 0 To dgVol.Rows.Count - 1
                    con = New SqlConnection(strCon)
                    Dim sql As String = "INSERT INTO " & secc & " (Seccion,Contratista,ItemV,Detalle,UnidadV,PrecioMM,
                PrecioSMO,SN,Contratado,Replanteado,Construido) values (
                '" & cbSeccion.Text & "',  '" & cbContr.Text & "',  '" & dgVol.Rows(i).Cells("Itemv").Value() & "', 
                '" & dgVol.Rows(i).Cells("Detalle").Value() & "',  '" & dgVol.Rows(i).Cells("Unidadv").Value() & "'
                ,  '" & dgVol.Rows(i).Cells("PrecioMM").Value() & "',  '" & dgVol.Rows(i).Cells("PrecioSMO").Value() & "'
                ,  '" & dgVol.Rows(i).Cells("SN").Value() & "',  '" & dgVol.Rows(i).Cells("Contratado").Value() & "'
                ,  '" & dgVol.Rows(i).Cells("Replanteado").Value() & "',  '" & dgVol.Rows(i).Cells("Construido").Value() & "')"
                    Dim comando As SqlCommand
                    comando = New SqlCommand(sql, con)
                    con.Open()
                    comando.ExecuteNonQuery()

                    con.Close()

                Next
            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                MessageBox.Show("Agregado Éxitosamente!", "Agregando", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try


        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "DELETE FROM " & secc & " "

            '"Select CodElemento From Elementos Where Item='" & ModificarM1.tbItem.Text & "'"
            '"Select CodElemento From Elementos Where Item='" & tbItem.Text & "'"
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            con.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show("Error Eliminación")
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Try
            Dim oExcel As New Excel.Application
            oExcel = CreateObject("Excel.Application")
            Dim oBook As Object = oExcel.Workbooks.Add("C:\Users\ASUS\Desktop\Proyecto Propietarios\Resultado\Prueba.xlsx")

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "select * from SECCION"

                'ITEM, DESCRIPCION, UNIDAD
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim ds As New DataSet
                Dim da As New SqlDataAdapter(comando)
                da.Fill(ds, "Seccion")

                With oExcel
                    .Range("A" + (14).ToString).Value = ds.Tables("Seccion").Rows(0).Item(1).ToString
                    .Range("A" + (25).ToString).Value = ds.Tables("Seccion").Rows(1).Item(1).ToString
                End With



            Catch ex As Exception
                MessageBox.Show("Error 1")
            Finally
                con.Close()
            End Try




            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "Select ITEMV,DETALLE,UNIDADV,SN,PRECIOMM,PRECIOSMO,CONTRATADO,REPLANTEADO,CONSTRUIDO from Canalización"

                'ITEM, DESCRIPCION, UNIDAD
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim ds As New DataSet
                Dim da As New SqlDataAdapter(comando)
                da.Fill(ds, "Canalización")

                For i As Integer = 0 To ds.Tables("Canalización").Rows.Count - 1
                    With oExcel
                        .Range("G" + (i + 15).ToString + ":J" + (i + 15).ToString).Merge(True)
                        .Range("K" + (i + 15).ToString + ":N" + (i + 15).ToString).Merge(True)
                        .Range("O" + (i + 15).ToString + ":R" + (i + 15).ToString).Merge(True)
                        .Range("A" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(0).ToString
                        .Range("B" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(1).ToString
                        .Range("C" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(2).ToString
                        .Range("D" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(3).ToString
                        .Range("E" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(4).ToString
                        .Range("F" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(5).ToString
                        .Range("G" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(6).ToString
                        .Range("K" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(7).ToString
                        .Range("O" + (i + 15).ToString).Value = ds.Tables("Canalización").Rows(i).Item(8).ToString


                    End With

                Next


            Catch ex As Exception
                MessageBox.Show("Error 2")
            Finally
                con.Close()
            End Try

            Try
                con = New SqlConnection(strCon)
                Dim sql As String = "Select ITEMV,DETALLE,UNIDADV,SN,PRECIOMM,PRECIOSMO,CONTRATADO,REPLANTEADO,CONSTRUIDO from RedFeeder"

                'ITEM, DESCRIPCION, UNIDAD
                Dim comando As SqlCommand
                comando = New SqlCommand(sql, con)
                con.Open()
                Dim ds As New DataSet
                Dim da As New SqlDataAdapter(comando)
                da.Fill(ds, "RedFeeder")

                For i As Integer = 0 To ds.Tables("RedFeeder").Rows.Count - 1
                    With oExcel
                        .Range("G" + (i + 26).ToString + ":J" + (i + 26).ToString).Merge(True)
                        .Range("K" + (i + 26).ToString + ":N" + (i + 26).ToString).Merge(True)
                        .Range("O" + (i + 26).ToString + ":R" + (i + 26).ToString).Merge(True)
                        .Range("A" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(0).ToString
                        .Range("B" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(1).ToString
                        .Range("C" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(2).ToString
                        .Range("D" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(3).ToString
                        .Range("E" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(4).ToString
                        .Range("F" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(5).ToString
                        .Range("G" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(6).ToString
                        .Range("K" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(7).ToString
                        .Range("O" + (i + 26).ToString).Value = ds.Tables("RedFeeder").Rows(i).Item(8).ToString

                    End With
                Next


            Catch ex As Exception
                MessageBox.Show("Error 2")
            Finally
                con.Close()
            End Try




            With oExcel
                .Range("J6").Value = "" & cbContr.Text & ""

            End With



            oBook.SaveAs("C:\Users\ASUS\Desktop\Proyecto Propietarios\Resultado\Prueba.xlsx")
            oBook = Nothing
            oExcel.Quit()
            oExcel = Nothing
            GC.Collect()
            GC.WaitForPendingFinalizers()

        Catch ex As Exception

        End Try


        'oExcel = CreateObject("Excel.Application")
        'oExcel.Workbooks.Open("C:\Users\ASUS\Desktop\Proyecto Propietarios\Resultado\Prueba.xlsx")
        'oExcel.Visible = True



    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnCambiar.Click
        cbContr.Enabled = True
        btnCambiar.Visible = False
    End Sub

    Private Sub cbContr_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbContr.SelectedValueChanged

        cbContr.Enabled = False
        If cbContr.Enabled = False Then
            btnCambiar.Visible = True
        End If

    End Sub

    Private Sub VolumenM3_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Salir(e)
    End Sub

    Private Sub cbContr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbContr.SelectedIndexChanged
        tbBuscar.Enabled = True

        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "select ITEM,AREA,DESCRIPCION,UNIDAD,CUMM,CUSMO from ELEMENTO"
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            con.Open()
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(comando)

            da.Fill(dt)

            dgAdd.DataSource = dt


        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            con.Close()
        End Try



        dgAdd.Columns(0).Width = 50
        dgAdd.Columns(1).Width = 90
        dgAdd.Columns(2).Width = 380

        cbSeccion.Enabled = True


    End Sub

    Private Sub cbSeccion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSeccion.SelectedIndexChanged
        If two = False Then
            Dim GB As New DataGridViewButtonColumn
            GB.DataPropertyName = "Agregar"
            GB.UseColumnTextForButtonValue = True
            GB.Text = "Agregar"
            GB.HeaderText = "Agregar"
            dgAdd.Columns.AddRange(GB)
            dgAdd.Columns(6).Width = 70
            two = True
        End If



        Button4.Enabled = True
        tbBuscar.Enabled = True
        Button2.Enabled = True
        btnModificar.Enabled = True

        secc = cbSeccion.Text.Replace(" "c, String.Empty)


        Try
            con = New SqlConnection(strCon)
            Dim sql As String = "select ITEMV,DETALLE,UNIDADV,PRECIOMM,PRECIOSMO,SN,CONTRATADO,REPLANTEADO,CONSTRUIDO from " & secc & ""
            Dim comando As SqlCommand
            comando = New SqlCommand(sql, con)
            con.Open()
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(comando)

            da.Fill(dt)

            dgVol.DataSource = dt

        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            con.Close()
        End Try

        If one = False Then

            dgVol.Columns(0).HeaderText = "ITEM"
            dgVol.Columns(1).HeaderText = "DETALLE"
            dgVol.Columns(2).HeaderText = "UNIDAD"
            dgVol.Columns(3).HeaderText = "CUMM"
            dgVol.Columns(4).HeaderText = "CUSMO"
            dgVol.Columns(5).HeaderText = "SN"
            dgVol.Columns(6).HeaderText = "CONTRATADO"
            dgVol.Columns(7).HeaderText = "REPLANTEADO"
            dgVol.Columns(8).HeaderText = "CONSTRUIDO"


            Dim GB1 As New DataGridViewButtonColumn
            GB1.DataPropertyName = "Eliminar"
            GB1.UseColumnTextForButtonValue = True
            GB1.Text = "Eliminar"
            GB1.HeaderText = "Eliminar"
            dgVol.Columns.AddRange(GB1)
            dgVol.Columns(0).Width = 45
            dgVol.Columns(1).Width = 190
            dgVol.Columns(2).Width = 55
            dgVol.Columns(3).Width = 50
            dgVol.Columns(4).Width = 50
            dgVol.Columns(5).Width = 30

            one = True
        End If


    End Sub
End Class